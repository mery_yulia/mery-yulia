import 'package:flutter/material.dart';
import 'post_api.dart';
import 'get_api.dart';

class DataScreen extends StatefulWidget {
  @override
  _DataScreenState createState() => _DataScreenState();
}

class _DataScreenState extends State<DataScreen> {
  PostResult postResult;
  UserGet userGet;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('9').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Text('DataScreen'),
          ),
          body: Center(
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.blue[100],
              alignment: Alignment.center,
              padding: EdgeInsets.all(30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "WELCOME!",
                    style: TextStyle(fontSize: 48, color: Color(0xff0563BA)),
                  ),
                  Container(
                      child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(70.0),
                        child: Image.network(
                          userGet.avatar,
                          cacheHeight: 128,
                          cacheWidth: 128,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          userGet.firstName + " " + userGet.lastName,
                          style:
                              TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                        ),
                      ),
                      Text(
                        userGet.email,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                      )
                    ],
                  )),
                ],
              ),
            ),
          )),
    );
  }
}
