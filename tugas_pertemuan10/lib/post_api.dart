// ignore: unused_import
import 'package:http/http.dart' as http;
// ignore: unused_import
import 'dart:convert';

class PostResult {
  String id;
  String firstName;
  String lastName;
  String email;
  String avatar;

  PostResult({this.id, this.firstName, this.lastName, this.email, this.avatar});

  factory PostResult.createPostResult(Map<String, dynamic> object) {
    //return object PostResult yang baru
    return PostResult(
      id: object['id'].toString(),
      firstName: object['first_name'],
      lastName: object['last_name'],
      email: object['email'],
      avatar: object['avatar'],
    );
  }
}
